import { Component, OnInit } from '@angular/core';
import { EmpleosService } from '../../services/empleos.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-empleos',
  templateUrl: './lista-empleos.page.html',
  styleUrls: ['./lista-empleos.page.scss'],
})
export class ListaEmpleosPage implements OnInit {

  empleados: Observable<any[]>;

  citas: any[];

  empleados2: any[]


  cosas: any = new Array()

  constructor(private empleosService: EmpleosService,
    public router: Router) { }

  async ngOnInit() {
    this.empleados = this.empleosService.getEmpleos2();
 
    this.empleados.subscribe(data => {
      
      for(let aux of data){
        aux.iess = aux.salario * 0.095;
        if(aux.salario>1000)
          aux.class = "salarioAlto";
        else  
          aux.class = '';
      }
      this.empleados2 = data;
    });

    this.cosas.push({id: 10, nombre: "xya"})
    this.cosas.push({id: 20, nombre: "iuyiou"})
    this.cosas.push({id: 30, nombre: "opiopi"})

    await this.loadCitas();



  }

  async loadCitas(){
    this.empleosService.getCitas().subscribe(async data => {
      for(let aux of data){
        
        const usuario = await this.empleosService.getUserById(aux.user);
        console.log(aux.user, usuario);   
        aux.nombre = usuario.nombre;
      }

      this.citas = data;
    })
  }


  showEmpleo(id: any){
    this.router.navigate([`empleo/${id}`])
  }

  trackByFn(index, obj) {
    return obj.uid;
  }

  showCrearEmpleo(){
    this.router.navigate([`crear-empleo`])
  }

}

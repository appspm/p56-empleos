// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig: {
    apiKey: "AIzaSyAMaNyzBGSZDD3WK9StCczMrztwZpk6wsg",
    authDomain: "empleos-2020.firebaseapp.com",
    databaseURL: "https://empleos-2020.firebaseio.com",
    projectId: "empleos-2020",
    storageBucket: "empleos-2020.appspot.com",
    messagingSenderId: "842633412531",
    appId: "1:842633412531:web:ddef022c28354ce32195c6"
  },

  googleWebClientId: '842633412531-m2ggj3f6vbh7a8kktvv0rja2eb5gti2h.apps.googleusercontent.com'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
